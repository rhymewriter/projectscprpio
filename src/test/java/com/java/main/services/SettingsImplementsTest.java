package com.java.main.services;

import com.java.main.models.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class SettingsImplementsTest {

    private Settings settings;

    @Before
    public void init(){
      settings = new SettingsImplements();
    }

    @Test
    public void create() throws IOException {
       String[] params =  new String[]{"create", "ttt", "csv"};
        Person person = new Person("1", "2", "3", "4", "5");
        Person person1 = new Person("1", "2", "3", "4", "7");
        Person person2 = new Person("1", "2", "3", "4", "8");
       settings.create(person2, params);
       settings.create(person1, params);
       settings.create(person, params);


    }

    @Test
    public void delete() throws IOException {
        String[] params =  new String[]{"delete", "test", "json"};
        Person person = new Person("1", "2", "3", "4", "6");
        boolean actual = settings.delete("6", params);
        Assert.assertTrue(actual);
    }
}