package com.java.main.services;

import com.java.main.models.Person;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Console {
    private static final String MSG_WELCOME = "Добро пожаловать в приложение - \"Картотека личностей\"\n" +
                                             "____________________________________________________";
    private static final String MSG_ENVIRONMENT_LIST = "Выберите среду из списка: BINARY, JSON, CSV, XML, YAML";
    private static final String COMMAND_HELP = "Введите команду \"help\" для вызова списка доступных команд";
    private static final String COMMAND_HELP_LIST = "help - выводит список доступных команд\n" +
                                                    "exit - закрывает приложение\n" +
                                                    "switch - меняет среду хранения файлов\n" +
                                                    "start - запускает запись данных в файл\n" +
                                                    "cancel - отменяет запись данных в файл\n" +
                                                    "create - команда создания нового пользователя\n" +
                                                    "read - команда чтения данных из файлов\n" +
                                                    "update - команда внесения изменений в данные пользователя\n" +
                                                    "delete - команда удаления из файла сохранённых пользователей";
    private static final String COMMAND_CRUD = "или команды CRUD с указанием имени файла для начала работы";

    private static final Validator validator = new ValidatorImplements();

    private final Settings settings = new SettingsImplements();

    public static void msgWelcome() {
        System.out.println(MSG_WELCOME);
    }

    public void consoleInPerson(Scanner scanner) throws IOException {

        boolean retry = true;
        System.out.println(MSG_ENVIRONMENT_LIST);
        String scanExchangeFormat = scanner.nextLine();
        while (validator.checkFormat(scanExchangeFormat)) {
            System.out.println(MSG_ENVIRONMENT_LIST);
            scanExchangeFormat = scanner.nextLine();
        }

        while (retry) {
            System.out.println(COMMAND_HELP);
            System.out.println(COMMAND_CRUD);
            String scanCommandInput = scanner.nextLine();

            String[] commandAndFileName;
            commandAndFileName = scanCommandInput.split(" ");
            System.out.println(Arrays.toString(commandAndFileName));
//создание массива с параметрами (команда, имя файла)
            String[] params = new String[3];
            params[0] = commandAndFileName[0];

            String command = commandAndFileName[0];

            if (command != null || validator.chekInputFormatInArray(commandAndFileName) || validator.isNuLL(commandAndFileName)) {

                switch (command.toLowerCase()) {
                    case "create": {
                        if(!validator.isNull(commandAndFileName, 2)) {
                            params[1] = commandAndFileName[1];
                            params[2] = scanExchangeFormat;
                            retry = createInPerson(scanner, params);
                        }
                        break;
                    }
                    case "read": {
                        if(!validator.isNull(commandAndFileName, 2)) {
                            params[1] = commandAndFileName[1];
                            params[2] = scanExchangeFormat;
                            retry = readFile(scanner, params);
                        }
                        break;
                    }
                    case "update": {
                        if(!validator.isNull(commandAndFileName, 2)) {
                            params[1] = commandAndFileName[1];
                            params[2] = scanExchangeFormat;
                            retry = updatePerson(scanner, params);
                        }
                        break;
                    }
                    case "delete": {
                        if(!validator.isNull(commandAndFileName, 2)) {
                            params[1] = commandAndFileName[1];
                            params[2] = scanExchangeFormat;
                            retry = deletePersonInFile(scanner, params);
                        }
                        break;
                    }

                    case "help": {
                        System.out.println(COMMAND_HELP_LIST);
                        break;
                    }
                    case "exit": {
                        return;
                    }
                    case "switch": {
                        consoleInPerson(scanner);
                        break;
                    }
                    default:
                        System.out.println("Некорректный ввод команды");
                }
            } else {
                System.out.println("Некорректный ввод команды");
            }
        }
    }

    /**
     * Метод считывания данных от пользователя. Заполнение полей эеземпляра класса Person
     * @param scanner считка данных от пользователя
     * @param params список параметров для записи (тип команды, имя файла, выбранная среда)
     * @return (start) вызов метода create и передача входных данных;
     *          true (cancel) отмена, переход к выбору среды;;
     *          false ошибка при создании; некорректный ввод
     * @throws IOException
     */
    private boolean createInPerson(Scanner scanner, String[] params) throws IOException {
        boolean retry = true;
        String[] data = null;
        while (retry) {

//ввод пользователем полей нового экземпляра класса Person

            System.out.println("Введите: fname, lname, age, city, serialPassport");

            String info = scanner.nextLine();

            data = info.split(" ");

            if (validator.isNull(data, 5)) {
                System.out.println("Некорректный ввод информации");

            } else {
                retry = false;
            }
        }

// создание объекта Person поля которого заполняються из массива data;
        Person person = Person.createPersonArray(data);

        System.out.println("Вы действительно хотите записать в файл " + params[1] + "." + params[2] + " эти данные?");
        System.out.println(person);
        System.out.println("start - да\ncancel - отмена");

        String commandStartCancel = scanner.nextLine();
        while (!checkStartCancel(commandStartCancel)) {
            System.out.println("Некорректный ввод команды. Пожалуйста повторите.");
            commandStartCancel = scanner.nextLine();
        }
        switch (commandStartCancel) {
            case "start": {
                return settings.create(person, params);
            }
            case "cancel": {
                consoleInPerson(scanner);
                return true;
            }
            default:
                return false;

        }
    }

    private boolean checkStartCancel(String command) {
        return command.equalsIgnoreCase("cancel") || command.equalsIgnoreCase("start");
    }

    /**
     *
     * @param scanner считка данных от пользователя
     * @param params список параметров для записи (тип команды, имя файла, выбранное окружение)
     *  (start) вызов метода read - считыване и запись данных в массив Person[];
     *         (cancel) отмена, переход к выбору среды;
     * @return false - если не смогли считать фаил true - если считали;
     *
     * @throws IOException
     */
    private boolean readFile(Scanner scanner, String[] params) throws IOException {
        System.out.println("Вы действительно хотите считать фаЙл " + params[1] + "." + params[2] + " ?");
        System.out.println("start - да\nexit - выход");
        String commandStartCancel = scanner.nextLine();
        while (!checkStartCancel(commandStartCancel)) {
            System.out.println("Некорректный ввод команды. Пожалуйста повторите");
            commandStartCancel = scanner.nextLine();
        }
        switch (commandStartCancel) {
            case "start": {
                Person[] people = settings.read(params);
                if (people == null) {
                    System.out.println("Файл не найден");
                    return true;
                } else {
                    System.out.println(Arrays.toString(people));
                    System.out.println("\n");
                    return true;
                }
            }
            case "cancel": {
                consoleInPerson(scanner);
                return true;
            }
            default:
                return false;
        }
    }

    /**
     *
     * @param scanner считка данных от пользователя
     * @param params список параметров для записи (тип команды, имя файла, выбранная среда)
     * @return (start) вызов метода delete - передача параметов в метод (серия паспорта);
     *         true (cancel) отмена, переход к выбору среды;
     *         false некорректный ввод
     * @throws IOException
     */
    private boolean deletePersonInFile(Scanner scanner, String[] params) throws IOException {

        System.out.println("Введите серию паспорта");
        String serialPasport = scanner.nextLine();
        serialPasport = serialPasport == null ? "" : serialPasport;
        while (serialPasport.isEmpty()) {
            System.out.println("Некорректный ввод данных");
            System.out.println("Введите серию паспорта");
            serialPasport = scanner.nextLine();
        }

        System.out.println("Вы действительно хотите удалить пользователя с серией паспорта " + serialPasport + " ?");
        System.out.println("start - да\nexit - выход");
        String commandStartCancel = scanner.nextLine();
        while (!checkStartCancel(commandStartCancel)) {
            System.out.println("Некорректный ввод команды. Пожалуйста повторите");
            commandStartCancel = scanner.nextLine();
        }
        switch (commandStartCancel) {
            case "start": {
                return settings.delete(serialPasport, params);
            }
            case "cancel": {
                consoleInPerson(scanner);
                return true;
            }
            default:
                return false;
        }

    }

    /**
     *
     * @param scanner считка данных от пользователя
     * @param params список параметров для записи (тип команды, имя файла, выбранная среда)
     * @return
     * @throws IOException
     */
    private boolean updatePerson(Scanner scanner, String[] params) throws IOException {
        System.out.println("Введите серию паспорта");
        String serialPasport = scanner.nextLine();
        serialPasport = serialPasport == null ? "" : serialPasport;
        while (serialPasport.isEmpty()) {
            System.out.println("Некорректный ввод данных");
            System.out.println("Введите серию паспорта");
            serialPasport = scanner.nextLine();
        }
// заходим в файл и вычитываем данные в массив people
        Person[] people = settings.read(params);
        if (people == null) {
            System.out.println("Файл не найден");
            return false;
        }
        Person updatePerson = null;
        for (Person person : people) {
            if (person.getSerialPassport().equalsIgnoreCase(serialPasport)) {
                updatePerson = person;
            }
        }
        if (updatePerson == null) {
            System.out.println("Пользователь с данной серией паспорта - " + serialPasport + " не найден");
            return false;
        }
// персона найдена по паспортным данным
        System.out.println("Какие параметры Вы бы хотели изменить?");
        System.out.println("Напишите параметр и его значение через пробел : fname, lname, age, city, serialPassport ");

        String personInputLine = scanner.nextLine();
        String[] personParams = personInputLine.split(" ");
        String personParam = personParams[0];
        String personValue = personParams[1];

        while (validator.checkPersonParams(personParam, personValue)) {
            System.out.println("Какие параметры Вы бы хотели изменить?");
            System.out.println("Напишите параметр и его значение через пробел : fname, lname, age, city, serialPassport ");
            personInputLine = scanner.nextLine();
            personParams = personInputLine.split(" ");
            personParam = personParams[0];
            personValue = personParams[1];
        }

        switch (personParam) {
            case "fname": {
                updatePerson.setfName(personValue);
                break;
            }
            case "lname": {
                updatePerson.setlName(personValue);
                break;
            }
            case "age": {
                updatePerson.setAge(personValue);
                break;
            }
            case "city": {
                updatePerson.setCity(personValue);
                break;
            }
            case "serialPassport": {
                updatePerson.setSerialPassport(personValue);
                break;
            }
        }

        System.out.println("Вы действительно хотите изменить  пользователя " + updatePerson + " ?");
        System.out.println("start - да\ncancel - отмена");
        String command = scanner.nextLine();

        while (!checkStartCancel(command)) {
            System.out.println("Некорректный ввод команды. Пожалуйста повторите");
            command = scanner.nextLine();
        }


        return settings.update(people, params);


    }

}

