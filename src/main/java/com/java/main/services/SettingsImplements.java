package com.java.main.services;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvFactory;
import com.fasterxml.jackson.dataformat.xml.XmlFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.java.main.models.Person;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class SettingsImplements implements Settings {

    private static final ObjectMapper mapperBinary = new ObjectMapper();
    private static final ObjectMapper mapperJson = new ObjectMapper(new JsonFactory());
    private static final ObjectMapper mapperCsv = new ObjectMapper(new CsvFactory());
    private static final ObjectMapper mapperXml = new ObjectMapper(new XmlFactory());
    private static final ObjectMapper mapperYaml = new ObjectMapper(new YAMLFactory());


    /**
     * Метод записи пользователя в новый файл, добавление пользователя и перезапись файла
     *
     * @param person - обьект который будет записыватся в фаил
     * @param params список параметров для записи (тип команды, имя файла, окружение)
     * @return true при успешной записи файла, false при отказе
     * @throws IOException
     */
    @Override
    public boolean create(Person person, String[] params) throws IOException {
        String fileName = params[1];
        String formatFile = params[2].toLowerCase();
        ObjectMapper mapper = getMapper(params[2]);
//проверка выбранной среды на null
        if (mapper == null) {
            System.out.println("Неверный формат");
            return false;
        }
//проверка - существует ли файл по указанному пути
        Person[] people = null;
        try {
            if (formatFile.equalsIgnoreCase("binary")) {
                byte[] bytes = mapper.readValue(new File("src/main/resources/" + fileName + "." + formatFile), byte[].class);
                people = mapper.readValue(bytes, Person[].class);
            } else {
                people = mapper.readValue(new File("src/main/resources/" + fileName + "." + formatFile), Person[].class);
            }
        } catch (FileNotFoundException e) {
//            System.out.println("информация успешно записана в файл " + fileName + "." + formatFile);
        }
// проверка файла на наличие массива c Person-ами
        if (people == null) {
            people = new Person[]{person};
// проверка файла на наличие Person которого мы хотим туда записать
        } else {
            if (checkEqualsPerson(people, person.getSerialPassport())) {
                System.out.println("Пользователь с такой серией паспорта уже существует\n");
                return true;
            }
// увеличивание массива на 1 ячейку, запись в последнюю ячейку объекта Person
            people = Arrays.copyOf(people, people.length + 1);
            people[people.length - 1] = person;

        }
        if (formatFile.equalsIgnoreCase("binary")) {
            byte[] bytes = mapper.writeValueAsBytes(people);
            mapper.writeValue(new File("src/main/resources/" + fileName + "." + formatFile), bytes);

        } else {
            mapper.writeValue(new File("src/main/resources/" + fileName + "." + formatFile), people);

        }
        System.out.println("Информация успешно записана в файл " + fileName + "." + formatFile + "\n");
        return true;
    }

    /**
     * Метод для считывания данных из файла
     *
     * @param params список параметров для считывания файла (тип команды, имя фаила, окружение)
     * @return возвращает массив с данными Person если файл считан, возвращает null если файл не существует
     * @throws IOException
     */
    @Override
    public Person[] read(String[] params) throws IOException {
        String fileName = params[1];
        String formatFile = params[2].toLowerCase();
        ObjectMapper mapper = getMapper(params[2]);
        Person[] people;
        try {
            if (formatFile.equalsIgnoreCase("binary")) {
                byte[] bytes = mapper.readValue(new File("src/main/resources/" + fileName + "." + formatFile), byte[].class);
                people = mapper.readValue(bytes, Person[].class);
            } else {
                people = mapper.readValue(new File("src/main/resources/" + fileName + "." + formatFile), Person[].class);
            }
        } catch (FileNotFoundException e) {
            return null;
        }
        return people;
    }

    /**
     * @param people массив обновлённых данных для записи
     * @param params список параметров для записи обновлённых данных файла (тип команды, имя фаила, окружение)
     * @return
     * @throws IOException
     */
    @Override
    public boolean update(Person[] people, String[] params) throws IOException {
        String fileName = params[1];
        String formatFile = params[2].toLowerCase();
        ObjectMapper mapper = getMapper(params[2]);
        if (formatFile.equalsIgnoreCase("binary")) {
            byte[] bytes = mapper.writeValueAsBytes(people);
            mapper.writeValue(new File("src/main/resources/" + fileName + "." + formatFile), bytes);

        } else {
            mapper.writeValue(new File("src/main/resources/" + fileName + "." + formatFile), people);
        }
        System.out.println("Успешное внесение изменений " + fileName + "." + formatFile + "\n");
        return true;
    }

    /**
     * @param serialNumber номер паспорта, для поиска конкретного Person
     * @param params       список параметров для чтения данных файла (тип команды, имя фаила, окружение)
     * @return
     * @throws IOException
     */
    @Override
    public boolean delete(String serialNumber, String[] params) throws IOException {
        String fileName = params[1];
        String formatFile = params[2].toLowerCase();
        Person[] people = read(params);

        if (people == null) {
            System.out.println("Чтение файла не возможно\n");
            return true;
        }

        if (!checkEqualsPerson(people, serialNumber)) {
            System.out.println("Данная личность не существует\n");
            return true;
        }

        int arrayIndex = getArrayIndex(people, serialNumber);

        Person[] newPeople = new Person[people.length - 1];
        for (int i = 0; i < arrayIndex; i++) {
            newPeople[i] = people[i];
        }
        int length = people.length;
        for (int i = arrayIndex; i < newPeople.length; i++) {
            newPeople[i] = people[i + 1];
        }

        ObjectMapper mapper = getMapper(params[2]);

        if (formatFile.equalsIgnoreCase("binary")) {
            byte[] bytes = mapper.writeValueAsBytes(people);
            mapper.writeValue(new File("src/main/resources/" + fileName + "." + formatFile), bytes);

        } else {
            mapper.writeValue(new File("src/main/resources/" + fileName + "." + formatFile), newPeople);
        }
        System.out.println("успешное удаление личности из файла " + fileName + "." + formatFile + "\n");
        return true;

    }

    /**
     * @param format определение окружения введённого пользователем
     * @return возврат конкретного окружения в объект
     */
    private ObjectMapper getMapper(String format) {
        if (format == null) {
            return null;
        }
        switch (format.toUpperCase()) {
            case "JSON": {
                return mapperJson;
            }
            case "XML": {
                return mapperXml;
            }
            case "CSV": {
                return mapperCsv;
            }
            case "YAML": {
                return mapperYaml;
            }
            case "BINARY":
                return mapperBinary;
            default:
                return null;
        }
    }

    /**
     * @param peoples      массив с объектами Person
     * @param serialNumber уникальный идентификатор (серия паспорта)
     * @return true - совпадение значения паспорта; false - нет
     */
    private static boolean checkEqualsPerson(Person[] peoples, String serialNumber) {
        for (Person people : peoples) {
            if (people.getSerialPassport().equalsIgnoreCase(serialNumber)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param people       массив с объектами Person
     * @param serialNumber уникальный идентификатор (серия паспорта)
     * @return возврат индекса Person
     */

    private int getArrayIndex(Person[] people, String serialNumber) {
        for (int i = 0; i < people.length; i++) {
            if (people[i].getSerialPassport().equals(serialNumber)) {
                return i;
            }
        }
        return 0;
    }
}


