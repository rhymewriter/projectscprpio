package com.java.main.services;

public interface Validator {
    default boolean isNuLL( String[] params) {
       if (params == null || params.length == 0){
            return false;
        }
        for (String param : params) {
            if (param == null || param.isEmpty()){
                return false;

            }
        }
        return true;
    }
    default boolean isNull (String[] params, int size) {
        if (params.length != size) {
            return true;
        }
        for (String param : params) {
            if (param == null || param.trim().isEmpty()) {
                return true;
            }
        } return false;

    }
    boolean chekInputFormatInArray(String[] params);
    boolean checkFormat(String format);
    boolean checkPersonParams(String param, String value);
}
