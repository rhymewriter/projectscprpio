package com.java.main.services;

public class ValidatorImplements implements Validator {
    private static final String[] formats = new String[] {"BINARY", "JSON", "CSV", "XML", "YAML"};
    private static final String[] personParams = new String[]{"fname", "lname", "age", "city", "serialPassport"};

    /**
     *
     * @param params
     * @return
     */
    @Override
    public boolean chekInputFormatInArray(String[] params) {
        if(params.length < 3) {
            System.out.println("Некорректный ввод информации");
            return false;
        }
        for (String format: formats) {
            if(format.equals(params[2].toUpperCase())) {
                return true;
            }

        }
        System.out.println("Некорректный формат файла!");
        return false;
    }

    /**
     *
     * @param inputFormats
     * @return
     */
    @Override
    public boolean checkFormat(String inputFormats){
        for (String format: formats) {
        if(inputFormats.toUpperCase().equals(format)) {
            return false;
        }

    }
        System.out.println("Некорректный формат файла!");
        return true;
    }

    /**
     *
     * @param param поля экземпляра объекта Person (валидация введеных пользоватлем команд + null + пустота)
     * @param value значение параметра вносимого пользователем (проверка на null и на пустую строку)
     * @return true команда найдена, а вносимоё значение не пустое; false - одна из проверок завалена
     */
    @Override
    public boolean checkPersonParams(String param, String value) {
       if (param == null || param.isEmpty() || value == null || value.isEmpty()){
           return true;
       }
        for(String p : personParams){
            if (p.equalsIgnoreCase(param)){
                return false;
            }
        }
        return true;
    }
}
