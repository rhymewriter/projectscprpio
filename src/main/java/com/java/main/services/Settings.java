package com.java.main.services;

import com.java.main.models.Person;

import java.io.IOException;

public interface Settings {
    boolean create (Person person, String[] params) throws IOException;
    Person[] read (String[] params) throws IOException;
    boolean update (Person[] person, String[] params) throws IOException;
    boolean delete (String serialNumber, String[] params) throws IOException;
}
