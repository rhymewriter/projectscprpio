package com.java.main.models;

import java.util.Objects;

public class Person {

    private String fName;
    private String lName;
    private String city;
    private String age;
    private String serialPassport;
    private int id;
    private static int staticId;

    public Person() {
    }

    public Person(String fName, String lName, String age, String city, String serialPassport) {
        this.fName = fName;
        this.lName = lName;
        this.city = city;
        this.age = age;
        this.id = staticId++;
        this.serialPassport = serialPassport;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getCity() {
        return city;
    }

    public String getAge() {
        return age;
    }

    public int getId() {
        return id;
    }

    public String getSerialPassport() {
        return serialPassport;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSerialPassport(String serialPassport) {
        this.serialPassport = serialPassport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return fName.equals(person.fName) && lName.equals(person.lName) && city.equals(person.city) && age.equals(person.age) && serialPassport.equals(person.serialPassport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fName, lName, city, age, serialPassport);
    }

    @Override
    public String toString() {
        return "Person{" +
                "fName='" + fName + '\'' +
                ", lName='" + lName + '\'' +
                ", city='" + city + '\'' +
                ", age='" + age + '\'' +
                ", serialPassport='" + serialPassport + '\'' +
                '}';
    }

    public static Person createPersonArray(String[] paramsPersons) {
        return new Person(paramsPersons[0], paramsPersons[1], paramsPersons[2], paramsPersons[3], paramsPersons[4]);

    }
}
