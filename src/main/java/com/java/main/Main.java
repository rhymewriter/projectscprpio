package com.java.main;

import com.java.main.services.Console;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Console console = new Console();
        console.msgWelcome();
        Scanner scanner = new Scanner(System.in);
        console.consoleInPerson(scanner);
    }
}
