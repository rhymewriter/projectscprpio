Приложение Картотека

Задача приоложения - хранение данных клиентов

Приложенние поддерживает хранение информации в таких форматах:
BINARY, JSON, YAML, CSV, XML

Возможности приложения
создание и сохранение информации о пользователях
просмотр информации о пользователях в выбранном файле
внесение изменений в параметры пользователя
удаление пользователя из указанного файла

Приложение поддерживает следующие команды:

help - выводит список доступных команд
exit - закрывает приложение
switch - меняет среду хранения файлов
start - запускает запись данных в файл
cancel - отменяет запись данных в файл
create - команда создания нового пользователя
read - команда чтения данных из файлов
update - команда внесения изменений в данные пользователя
delete - команда удаления из файла сохранённых личностей

Работа программы

При запуске приложение предлагает пользователю выбрать окружение:
BINARY, JSON, YAML, CSV, XML

После выбора окружения приложение переходит в основное меню
на данном этапе пользователь может ввести команду help
для вывода всех доступных каманд приложения или начать работу

Для начала работы нужно выбрать одну из  CRUD команд
ввести её в консоль и через пробел добавить имя файла 

Например: create newFile

На следующем этапе пользователю предлагаеться ввести поля
Фамилия Имя Возраст Город Серия паспорта через пробел

После внесения всей необходимой информации пользватель должен поддтвердить ввод
ввести команду start для отправки данных на запись в файл
в противном случае для олтмены ввести команду cancel

После основных манипуляций с данными пользователь возвращаеться к выбору среды

Над программой работали:
Алёна Грабчук - владычица морская, вдохновитель, планировщик и что то писала в проекте
Максим Тимошенко - копипастер, попрошайка, что то писал в проекте
Денис Берко - непризнаный гений, наставник, что то писал в проекте

Огромная благодарность Всем кто помогал и страдал за компанию. 

Особая благодарность:
Желанному Владиславу
Ковалёву Сергею
Жан Марку Кимбембе




